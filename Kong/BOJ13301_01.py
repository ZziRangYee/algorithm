import sys

num = int(sys.stdin.readline())

tileArr = []

tileArr.append(4)
tileArr.append(6)

for i in range(2, num):
    rectangle = tileArr[i-2] + tileArr[i-1]
    tileArr.append(rectangle)

print(tileArr[num-1])