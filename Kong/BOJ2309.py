import sys

dwarfArr = []

for i in range (0, 9):
    dwarfArr.append( int(sys.stdin.readline().rstrip('\n')) )

resultArr = []
breaker = False

for i in range (0, 8):
    for j in range(i+1, 9):
        temArr = dwarfArr[:]
        del temArr[j]
        del temArr[i]
        if sum(temArr) == 100:
            resultArr = temArr
            breaker = True

    if breaker == True:
        break

resultArr.sort()

for i in range (0, 7):
    print(resultArr[i])