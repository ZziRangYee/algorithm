import sys

loopNum = int(sys.stdin.readline())
numList = []

maxRes = 0

for i in range(loopNum):
    numList.append(float(sys.stdin.readline()))

for idx in range(loopNum):
    multiNum = numList[idx]
    for j in range(idx+1, loopNum):
        multiNum = multiNum * numList[j]

        if maxRes < multiNum:
            maxRes = multiNum

print(round(maxRes, 3))

# 타임아웃 발생