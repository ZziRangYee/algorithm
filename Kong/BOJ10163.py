import sys

colorPaperNum = int(sys.stdin.readline())

matrix = [[1]*101 for i in range(101)]

colorPaperInfo = []
for i in range(0, colorPaperNum):
    colorPaperInfo.insert(0, sys.stdin.readline().strip('\n').split(' '))

resultList = []
for i in range(0, colorPaperNum):
    area = 0

    pointX = int(colorPaperInfo[i][0])
    pointY = int(colorPaperInfo[i][1])
    width = int(colorPaperInfo[i][2])
    height = int(colorPaperInfo[i][3])

    for y in range(pointY, (pointY + height)):
        for x in range(pointX, (pointX + width)):
            if matrix[x][y] == 1:
                area += 1

            matrix[x][y] = 0

    resultList.insert(0, area)

for i in range(0, colorPaperNum):
    print(resultList[i])
