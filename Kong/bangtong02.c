#include <stdio.h>

void draw_ljh(int num); // 함수 선언

int main(void){
	printf("5부터 10 사이의 홀수 숫자를 입력하시오. \n");
	printf("(0을 입력시 프로그램 종료) \n");
	
	int num; // 변수 선언
	do{
		printf("숫자입력 : ");
		scanf("%d", &num); // 숫자 입력
		
		if(num == 0 ){ // 입력 숫자가 0인 조건
			printf("♥ 프로그램을 종료합니다. 이름 : 이종헌 \n");
		}else if(num < 5 || num > 10){ // 5 ~ 10 사이가 아니면 조건
			printf("☆ 5부터 10 사이의 홀수를 입력하시오. 학과 : 컴퓨터 과학과 \n");
		}else if(num % 2 == 0) { // 짝수이면 조건
			printf("★ 홀수를 입력하시오. 학번 : 201634-354902 \n");
		}else{ 
			draw_ljh(num); // 배열크기로 출력 
		}
		
	}while(num != 0); // 0이 아니면 계속 진행
	
    return 0;
}

void draw_ljh(int num){
	int inputNum, half, eNum, ePlusCnt; // 변수 선언

    inputNum = num; // 입력값 넣기.
    half = inputNum / 2 + 1; // 절반 지점 구하기
    eNum = 1; // 빈 부분 최소값
    ePlusCnt = inputNum + 1; // 빈 부분 최대값

    // 이중 반복문으로 형태 만들기
    for(int out = 1; out <= inputNum; out++){
        int calcNum = inputNum - 1; // 횟수 증가 때 마다 더해야 할 초기 값 지정
        int calcSum = out; // 출력 되어질 값 
        for(int in = 1; in <= inputNum; in++){
            if(in == 1){ // 첫번째 출력 하고 다음으로 넘어가도록 하는 조건.
                printf("%d ", calcSum);
                continue;
            }

            calcSum = calcSum + calcNum; // 출력 값 설정

            if(in > eNum && in < ePlusCnt - eNum){ // 빈 부분을 그려야 할 조건 지정
                printf("   ");
            }else{
                if( calcSum / 10 ==0){ // 1의 자리이면 앞에 공백 추가 해서 출력
                    printf(" %d ", calcSum);
                }else{ // 10의 자리 이상은 그냥 출력
                    printf("%d ", calcSum);
                }
            }

            if(in < half){ // 반복문 절반 아래면 더해야 할 값에 -2로 계산
                calcNum -= 2;
            }else if(in > half){ // 반복문 절반 위면 더해야 할 값에 +2로 계산
                calcNum += 2;
            }
        }

        if(out < half){ // 반복문 절반 아래면 빈값 시작점 1씩 증가
            eNum++;
        }else{ // 반복문 절반 위면 빈값 시작점 1씩 감소
            eNum--;
        }

        printf("\n");

    }
}

