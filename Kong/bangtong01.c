#include <stdio.h>
#include <string.h>
#include <ctype.h>

void change_ljh(char* str); // 함수 선언

int main(void){
	char inputStr[100]; // 입력 받은 문자열 크기 지정
	
    printf("문자열을 대소문자로 구분하여 입력 : "); 
    scanf("%[^\n]", inputStr); // 띄어쓰기도 포함해서 받기.
	
	change_ljh(inputStr); //함수 실행
	
	printf("변환된 결과물 출력 ==> %s \n", inputStr); // 출력
	
    return 0;
}

void change_ljh(char* str){
	size_t size = strlen(str);	// size 에 s의 크기를 저장
	char temp;					// 문자를 뒤집을때 필요한 빈공간

	//문자열 역순으로 만들기
	for (size_t i = 0; i < size / 2; i++) { // 문자열 크기의 절반만 순환문 돌도록 설정
    	temp = str[i]; // 임시변수에 값 추가
    	str[i] = str[(size - 1) - i]; // i번째 변수에 뒤에서 i번째 값 넣기.
    	str[(size - 1) - i] = temp; // 뒤에서 i번째 값에 임시변수 값 넣기.
  	}
	
	// 대소문자 변경
	for(int i=0; str[i]; i++){ // 문자열 크기 만큼 순환
		if( islower(str[i]) ){ // i번째 문자가 소문자 인지 확인
			str[i] = str[i]-'a'+'A'; // 대문자로 변환
		}else if( isupper(str[i]) ){ // i번째 문자가 대문자 인지 확인
			str[i] = str[i]-'A'+'a'; // 소문자로 변환
		}
	}
}

