import sys

playNum = int(sys.stdin.readline())

for i in range(0, playNum):
    result = 0
    card01 = {'4': 0, '3': 0, '2': 0, '1': 0}
    card02 = {'4': 0, '3': 0, '2': 0, '1': 0}

    player01 = sys.stdin.readline().strip('\n').split(' ')
    for idx in player01[1:]:
        card01[idx] = card01[idx] + 1

    player02 = sys.stdin.readline().strip('\n').split(' ')
    for idx in player02[1:]:
        card02[idx] = card02[idx] + 1


    for card in card01.keys():
        if card01[card] > card02[card]:
            result = 1
        if card01[card] < card02[card]:
            result = 2
        if result != 0:
            break

    resStr = 'D'
    if result == 1:
        resStr = 'A'
    if result == 2:
        resStr = 'B'

    print(resStr)