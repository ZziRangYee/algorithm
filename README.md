# Algorithm

[알고리즘]
- 알고리즘 문제는 풀고 개념공부
- 해커랭크 1문제(warmup제외)
- 백준1문제 풀기 (분류 -> 차례대로 ->70%이상)

공부순서 참고
- http://baactree.tistory.com/14
- http://joonas.tistory.com/39


/*** 18.10.08 ***/
- 백준 : https://www.acmicpc.net/problem/2698
- 해커랭크 : https://www.hackerrank.com/challenges/apple-and-orange/problem

/*** 18.10.16 ***/
- 완전탐색 관련 문제
- https://www.acmicpc.net/problem/2231
- https://www.acmicpc.net/problem/2309

/*** 18.11.13 ***/
- DP(초급) : https://kks227.blog.me/220777103650
- https://www.acmicpc.net/problem/9465
- https://www.acmicpc.net/problem/1463

/*** 19.03.21 ***/
 - https://www.acmicpc.net/problem/1309

```
19.04.01
https://www.acmicpc.net/problem/2718
```

/*** 19.04.08 ***/
- https://www.acmicpc.net/problem/14696
- https://www.acmicpc.net/problem/14697

/*** 19.04.15 **/
- https://www.acmicpc.net/problem/13301

/*** 19.04.22 ***/
- https://www.acmicpc.net/problem/2670

/*** 19.05.20 ***/
- https://www.acmicpc.net/problem/10163

/*** 19.6.24 ***/
- https://www.acmicpc.net/problem/15683

/*** 19.08.16 ***/
- https://www.acmicpc.net/problem/2262

